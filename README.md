# Early Genre Detection

Train a model to classify songs according to the genre they belong to. The model will be evaluated on both latency (early high confidence prediction) and accuracy. By early prediction, it is meant that the model should go through the audio temporally

## Results

You can find an (incomplete) sample of training stats and metrics (including system usage) on our [wandb dashboard](https://app.wandb.ai/ajayuppili/uncategorized/runs/usncmnm6?workspace=user-ajayuppili)